#! /bin/bash

# ZooKeeper main config for ensemble.

ips=("$@")
host_ip=`hostname -i | awk '{print $2}'`

echo "tickTime=2000" > /root/zookeeper/conf/zoo.cfg
echo "dataDir=/var/lib/zookeeper" >> /root/zookeeper/conf/zoo.cfg
echo "clientPort=2181" >> /root/zookeeper/conf/zoo.cfg
echo "initLimit=5" >> /root/zookeeper/conf/zoo.cfg
echo "syncLimit=2" >> /root/zookeeper/conf/zoo.cfg

for i in "${!ips[@]}"
do
        echo "server.${i}=${ips[$i]}:2888:3888" >> /root/zookeeper/conf/zoo.cfg

        if [[ "${ips[$i]}" == "${host_ip}" ]]
        then
                mkdir -p "/var/lib/zookeeper/${i}"
                echo "${i}" > "/var/lib/zookeeper/${i}/myid"
        fi
done

echo "autopurge.snapRetainCount=3" >> /root/zookeeper/conf/zoo.cfg
echo "autopurge.purgeInterval=1" >> /root/zookeeper/conf/zoo.cfg

# Enable logging.

echo 'ZOO_LOG_DIR="/var/log/zookeeper"' > /root/zookeeper/conf/zookeeper-env.sh
echo 'ZOO_LOG4J_PROP="INFO,ROLLINGFILE"' >> /root/zookeeper/conf/zookeeper-env.sh
echo 'SERVER_JVMFLAGS="-Xms2048m -Xmx2048m -verbose:gc -Xloggc:$ZOO_LOG_DIR/zookeeper_gc.log"' >> /root/zookeeper/conf/zookeeper-env.sh
echo 'JVMFLAGS="$JVMFLAGS -Djute.maxbuffer=50000000"' >> /root/zookeeper/conf/zookeeper-env.sh

